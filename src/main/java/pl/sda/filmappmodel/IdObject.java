package pl.sda.filmappmodel;

public class IdObject {

    String $oid;

    public IdObject(String $oid) {
        this.$oid = $oid;
    }

    @Override
    public String toString() {
        return  $oid;
    }

    public String get$oid() {
        return $oid;
    }

    public void set$oid(String $oid) {
        this.$oid = $oid;
    }
}
