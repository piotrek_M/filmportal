package pl.sda.filmappmodel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

public class Actor {

    private int id;

    String content;

    private String firstName;

    private String lastName;

    private String birthDate;

    private Date passingDate;

    private double rating;

    private Set<Film> films;

    public Actor() {
    }

    public Actor(String contentFromJson){
        String[] split = contentFromJson.split(" ");
        this.firstName = split[0];
        this.lastName = split[1];
    }

    public Actor(int id, String firstName, String lastName, String birthDate, Date passingDate, double rating, Set<Film> films) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.passingDate = passingDate;
        this.rating = rating;
        this.films = films;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public Date getPassingDate() {
        return passingDate;
    }

    public void setPassingDate(Date passingDate) {
        this.passingDate = passingDate;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public Set<Film> getFilms() {
        return films;
    }

    public void setFilms(Set<Film> films) {
        this.films = films;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Actor actor = (Actor) o;

        if (Double.compare(actor.rating, rating) != 0) return false;
        if (!firstName.equals(actor.firstName)) return false;
        if (!lastName.equals(actor.lastName)) return false;
        if (!birthDate.equals(actor.birthDate)) return false;
        if (passingDate != null ? !passingDate.equals(actor.passingDate) : actor.passingDate != null) return false;
        return films.equals(actor.films);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (passingDate != null ? passingDate.hashCode() : 0);
        temp = Double.doubleToLongBits(rating);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (films != null ? films.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                ", passingDate=" + passingDate +
                ", rating=" + rating +
                ", films=" + films +
                content+
                '}';
    }
}
