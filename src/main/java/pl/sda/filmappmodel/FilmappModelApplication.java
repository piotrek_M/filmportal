package pl.sda.filmappmodel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilmappModelApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilmappModelApplication.class, args);
	}
}
