package pl.sda.filmappmodel;

import com.google.gson.annotations.SerializedName;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Film {

    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("director")
    private String director;
    @SerializedName("year")
    private int year;
    //@SerializedName("idGenre")
    private String idGenre;
    @SerializedName("rate")
    private String rate;
    @SerializedName("poster")
    private String posterUrl;
    @SerializedName("plot")
    private String plot;
    private Set<Actor> actorsSet;


    ///////////////////////////////////////////////
    private IdObject _id;
    private String[] actors;


    public Film build(){
        actorsSet = new HashSet<>();
        Arrays.asList(actors)
                .forEach(actor -> actorsSet.add(new Actor(actor)));
        return this;
    }

    //////////////////////////////////////////////



    public Film() {
    }

    public Film(int id, String title, String director, int year, String idGenre, String rate, String posterUrl, String plot, Set<Actor> actorsSet) {
        this.id = id;
        this.title = title;
        this.director = director;
        this.year = year;
        this.idGenre = idGenre;
        this.rate = rate;
        this.posterUrl = posterUrl;
        this.plot = plot;
        this.actorsSet = actorsSet;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getIdGenre() {
        return idGenre;
    }

    public void setIdGenre(String idGenre) {
        this.idGenre = idGenre;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public Set<Actor> getActors() {
        return actorsSet;
    }

    public void setActors(Set<Actor> actorsSet) {
        this.actorsSet = actorsSet;
    }

    @Override
    public String toString() {
        return "Film{" +
                "\nid=" + _id +
                ",\n title='" + title + '\'' +
                ", director='" + director + '\'' +
                ", year=" + year +
                ", idGenre='" + idGenre + '\'' +
                ", rate='" + rate + '\'' +
                ", posterUrl='" + posterUrl + '\'' +
                ", plot='" + plot + '\'' +
                ",\n actors=" + actorsSet +
                '}';
    }
}