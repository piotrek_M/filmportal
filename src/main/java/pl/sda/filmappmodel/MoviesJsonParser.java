package pl.sda.filmappmodel;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MoviesJsonParser {

    public static void main(String[] args) {

        String manyJsonsPath = "many.json";
        String oneJsonPath = "one.json";

        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx mnóstwo jsonów xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        parseMAnyJsonsFromFile(manyJsonsPath).forEach(System.out::println);
        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx jeden json xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
        System.out.println(parseOneJsonFromFile(oneJsonPath));
    }


    public static List<Film> parseMAnyJsonsFromFile(String filename) {

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        BufferedReader reader = null;

        List<Film> films = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            Gson gson = new GsonBuilder().create();
            Film[] movies = gson.fromJson(reader, Film[].class);
            films = Arrays.asList(movies);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return films
                .stream()
                .map(film -> film.build())
                .collect(Collectors.toList());
    }

    public static Film parseOneJsonFromFile(String filename) {

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        BufferedReader reader = null;

        Film film = null;
        try {
            reader = new BufferedReader(new FileReader(file));
            Gson gson = new GsonBuilder().create();
            film = gson.fromJson(reader, Film.class);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return film.build();
    }
}
